import { Box, makeStyles, TextField, Typography } from "@material-ui/core";
import { connect } from "react-redux";
import React, { useEffect, useState } from "react";
import { addBoard } from "../core/actions";
import { Link } from "react-router-dom";
import { getAllBoardStart } from "../core/actions";
import Header from "../components/Header";

const useStyle = makeStyles({
  homeContainer: {
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
  },
  thumbnail: {
    display: "flex",
    flexWrap: "wrap",
    marginBottom: 16,
    marginTop: 60,
  },
  linkContainer: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 3,
    boxShadow: "0px 2px 4px grey",
    margin: 8,
    padding: 8,
    textDecoration: "none",
    width: 120,
    border: "1px solid black",
    borderRadius: 3,
  },
  baordForm: {},
});

const AllBoards = (props) => {
  const [newBoardTitle, setNewBoardTitle] = useState("");
  const classes = useStyle();
  const { boards } = props;
  const handleChange = (e) => {
    setNewBoardTitle(e.target.value);
  };

  const handleFocus = (e) => {
    e.target.select();
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    if (newBoardTitle !== "") {
      props.addBoard({ newBoardTitle });
      setNewBoardTitle("");
    }
  };

  useEffect(() => {
    props.getAllBoardStart();
  }, []);

  return (
    <div className={classes.homeContainer}>
      <Header board = {true} />
      <div
        className={classes.thumbnail}
        sx={{ boxShadow: 3, borderColor: "black", border: 1 }}
      >
        {boards.map((board) => (
          <Link
            key={board.id}
            to={`/boards/${board.id}`}
            className={classes.linkContainer}
          >
            <Typography>{board.title}</Typography>
          </Link>
        ))}
      </div>

      <form onSubmit={handleSubmit} className={classes.baordForm}>
        <div>Create a new Board</div>
        <TextField
          onChange={handleChange}
          value={newBoardTitle}
          placeholder="Your boards title..."
          type="text"
          onBlur={handleSubmit}
          onFocus={handleFocus}
          autoFocus
        />
      </form>
    </div>
  );
};

const mapStateToProps = ({ boards }) => ({
  boards: boards.boards,
});

const mapDispatchToProps = (dispatch) => ({
  addBoard: ({ newBoardTitle }) => dispatch(addBoard({ newBoardTitle })),
  getAllBoardStart: () => dispatch(getAllBoardStart()),
});

export default connect(mapStateToProps, mapDispatchToProps)(AllBoards);
