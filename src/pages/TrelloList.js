import {
  Icon,
  makeStyles,
  TextField,
  Typography,
  CardContent,
} from "@material-ui/core";
import React, { useState } from "react";
import TrelloAddButton from "../components/TrelloAddButton";
import TrelloCard from "./TrelloCard/TrelloCard";
import { Droppable } from "react-beautiful-dnd";
import { Draggable } from "react-beautiful-dnd";
import { deleteList, editListTitle } from "../core/actions";
import { connect } from "react-redux";

const useStyle = makeStyles({
  listContainer: {
    backgroundColor: "#ebecf0",
    width: 280,
    padding: 8,
    marginTop: 8,
    borderRadius: 4,
  },
  titleContainer: {
    display: "flex",
    justifyContent: "space-between",
    cursor: "pointer",
    alignItems: "center",
    Width: "100%",
  },
  deleteListIcon: {
    transition: "opacity 0.3s ease-in-out",
    opacity: 0.4,
    "&:hover": {
      opacity: 0.8,
    },
  },
  titleSpace: {
    overflowWrap: "break-word",
    padding: 0,
    fontWeight: "bold",
  },
});

const TrelloList = (props) => {
  const { listId, title, cards, index } = props;
  const classes = useStyle();
  const [isEditing, setIsEditing] = useState(false);
  const [listTitle, setListTitle] = useState(title);
  console.log("CARDS IN TRELLOLIST...",cards);

  const handleFocus = (e) => {
    e.target.select();
  };

  const handleChange = (e) => {
    e.preventDefault();
    setListTitle(e.target.value);
  };

  const handleFinishEditing = () => {
    if (listTitle !== title) {
      props.editListTitle({ listId, listTitle });
    }
    setIsEditing(false);
  };

  const handleDeleteList = () => {
    props.deleteList({ listId });
  };

  const renderEditInput = () => {
    return (
      <form onSubmit={handleFinishEditing}>
        <TextField
          type="text"
          value={listTitle}
          onChange={handleChange}
          autoFocus
          onFocus={handleFocus}
          onBlur={handleFinishEditing}
        />
      </form>
    );
  };

  return (
    <Draggable draggableId={String(listId)} index={index}>
      {(provided) => (
        <div
          className={classes.listContainer}
          {...provided.draggableProps}
          {...provided.dragHandleProps}
          ref={provided.innerRef}
        >
          <Droppable droppableId={String(listId)} type="card">
            {(provided) => (
              <div>
                <div>
                  {isEditing ? (
                    renderEditInput()
                  ) : (
                    <div className={classes.titleContainer}>
                      <CardContent className={classes.titleSpace}>
                        <Typography
                          variant="body1"
                          component="h"
                          sx={{ fontWeight: "bolder" }}
                          onClick={() => setIsEditing(true)}
                        >
                          {title}
                        </Typography>
                      </CardContent>
                      <Icon
                        onClick={handleDeleteList}
                        fontSize="small"
                        className={classes.deleteListIcon}
                      >
                        delete
                      </Icon>
                    </div>
                  )}
                </div>
                <div {...provided.droppableProps} ref={provided.innerRef}>
                  <Typography variant="h6" component="h2">
                    {cards.map((card, index) => (
                      <div key={card.id}>
                        <TrelloCard
                          cardTitle={card.title}
                          cardDescription = {card.description}
                          id={card.id}
                          index={index}
                          listId={listId}
                          listTitle = {title}
                        />
                      </div>
                    ))}
                  </Typography>
                  <TrelloAddButton listId={listId} />
                  {provided.placeholder}
                </div>
              </div>
            )}
          </Droppable>
        </div>
      )}
    </Draggable>
  );
};

const mapStateToProps = ({ lists }) => ({
  showLoader: lists.showLoader,
});

const mapDispatchToProps = (dispatch) => ({
  editListTitle: ({ listId, listTitle }) =>
    dispatch(editListTitle({ listId, listTitle })),
  deleteList: ({ listId }) => dispatch(deleteList({ listId })),
});

export default connect(mapStateToProps, mapDispatchToProps)(TrelloList);
