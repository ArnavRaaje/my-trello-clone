import React, { useState, useEffect } from "react";
import { useNavigate } from "react-router";
import { Link } from "react-router-dom";
import trelloboard from "/home/arnav/FiftyFive Tech Projects/my-trello-board/src/assets/trello-logo-blue.svg";
import {
  Button,
  CssBaseline,
  TextField,
  Typography,
  Grid,
  Box,
  Container,
} from "@material-ui/core";
import { connect } from "react-redux";
import { emailSignInStart } from "../../core/actions";
import { fetchBoardDetailsStart } from "../../core/actions";
import { useStyle } from "./Styles/styles";

const Login = (props) => {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const navigate = useNavigate();
  const { currentUser } = props;
  console.log("current user in login props...", currentUser);
  const classes = useStyle();

  useEffect(() => {
    if (currentUser) {
      setEmail("");
      setPassword("");
      navigate("/boards");
    }
  }, [currentUser]);

  const handleSubmit = (event) => {
    event.preventDefault();
    props.emailSignInStart({ email, password });
  };

  return (
    <div className={classes.loginMainContainer}>
      <Container component="main" maxWidth="xs">
        <CssBaseline />
        <div className={classes.imageContainer}>
          <img
            src={trelloboard}
            alt="trello-logo-image"
            width="200px"
            height="auto"
          />
        </div>
        <Box className={classes.loginContainer}>
          <Typography component="h1" variant="h6" className={classes.loginText}>
            Log in to Trello
          </Typography>
          <Box
            component="form"
            onSubmit={handleSubmit}
            noValidate
            sx={{ mt: 1 }}
          >
            <TextField
              margin="normal"
              required
              fullWidth
              id="email"
              label="Enter Email"
              name="email"
              autoComplete="email"
              value={email}
              onChange={(e) => setEmail(e.target.value)}
              autoFocus
              className={classes.textfield}
            />
            <TextField
              margin="normal"
              required
              fullWidth
              name="password"
              label="Enter Password"
              type="password"
              value={password}
              onChange={(e) => setPassword(e.target.value)}
              id="password"
              autoComplete="current-password"
              className={classes.textfield}
            />
            <Button
              type="submit"
              fullWidth
              variant="contained"
              sx={{ mt: 3, mb: 2 }}
              className={classes.loginButton}
            >
              Log In
            </Button>
            <Grid container>
              <Grid item>
                <Link to="/signup" className={classes.link}>
                  Sign up for an account
                </Link>
              </Grid>
            </Grid>
          </Box>
        </Box>
      </Container>
    </div>
  );
};

const mapStateToProps = ({ user }) => ({
  currentUser: user.currentUser,
});

const mapDispatchToProps = (dispatch) => ({
  emailSignInStart: ({ email, password }) =>
    dispatch(emailSignInStart({ email, password })),
  fetchBoardDetailsStart: () => dispatch(fetchBoardDetailsStart()),
});

export default connect(mapStateToProps, mapDispatchToProps)(Login);
