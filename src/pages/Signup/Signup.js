import React, { useState, useEffect } from "react";
import { useNavigate } from "react-router";
import LockOutlinedIcon from "@material-ui/icons/LockOutlined";
import { Link } from "react-router-dom";
import trelloboard from "/home/arnav/FiftyFive Tech Projects/my-trello-board/src/assets/trello-logo-blue.svg";
import {
  Container,
  CssBaseline,
  Typography,
  Box,
  Grid,
  TextField,
  Button,
} from "@material-ui/core";
import { connect } from "react-redux";
import { emailSignUpStart } from "../../core/actions";
import { useStyle } from "./Styles/styles";

const Signup = (props) => {
  const [firstName, setFirstName] = useState("");
  const [lastName, setLastName] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  console.log("PROPS IN SIGN-UP...", props);
  const { currentUser } = props;
  const navigate = useNavigate();
  const classes = useStyle();
  useEffect(() => {
    if (currentUser) {
      setFirstName("");
      setLastName("");
      setEmail("");
      setPassword("");
      navigate("/");
    }
  }, [currentUser]);

  const handleSignUpSubmit = (event) => {
    event.preventDefault();
    props.emailSignUpStart({ firstName, lastName, email, password });
    console.log(firstName, lastName, email, password);
  };

  return (
    <div className={classes.loginMainContainer}>
      <Container component="main" maxWidth="xs">
        <CssBaseline />
        <div className={classes.imageContainer}>
          <img
            src={trelloboard}
            alt="trello-logo-image"
            width="200px"
            height="auto"
          />
        </div>
        <Box className={classes.loginContainer}
        >
          <Typography component="h1" variant="h5"className={classes.loginText}>
          Sign up for your account
          </Typography>
          <Box
            component="form"
            noValidate
            onSubmit={handleSignUpSubmit}
            sx={{ mt: 3 }}
          >
            <Grid container spacing={2}>
              <Grid item xs={12} sm={6}>
                <TextField
                  autoComplete="given-name"
                  name="firstName"
                  required
                  fullWidth
                  id="firstName"
                  value={firstName}
                  onChange={(e) => setFirstName(e.target.value)}
                  label="First Name"
                  autoFocus
                />
              </Grid>
              <Grid item xs={12} sm={6}>
                <TextField
                  required
                  fullWidth
                  id="lastName"
                  label="Last Name"
                  name="lastName"
                  value={lastName}
                  onChange={(e) => setLastName(e.target.value)}
                  autoComplete="family-name"
                />
              </Grid>
              <Grid item xs={12}>
                <TextField
                  required
                  fullWidth
                  id="email"
                  label="Email Address"
                  name="email"
                  value={email}
                  onChange={(e) => setEmail(e.target.value)}
                  autoComplete="email"
                />
              </Grid>
              <Grid item xs={12}>
                <TextField
                  required
                  fullWidth
                  name="password"
                  label="Password"
                  type="password"
                  id="password"
                  value={password}
                  onChange={(e) => setPassword(e.target.value)}
                  autoComplete="new-password"
                />
              </Grid>
            </Grid>
            <Button
              type="submit"
              fullWidth
              variant="contained"
              sx={{ mt: 3, mb: 2 }}
              className={classes.loginButton}
            >
              Sign Up
            </Button>
            <Grid container justifyContent="flex-end">
              <Grid item>
                <Link to="/"className={classes.link}>Already have an account? Sign in</Link>
              </Grid>
            </Grid>
          </Box>
        </Box>
      </Container>
    </div>
  );
};

const mapStateToProps = ({ user }) => ({
  currentUser: user.currentUser,
  userErr: user.userErr,
});

const mapDispatchToProps = (dispatch) => ({
  emailSignUpStart: ({ firstName, lastName, email, password }) =>
    dispatch(emailSignUpStart({ firstName, lastName, email, password })),
});

export default connect(mapStateToProps, mapDispatchToProps)(Signup);
