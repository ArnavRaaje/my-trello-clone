import { makeStyles } from "@material-ui/core";
import LoginBgImage from "/home/arnav/FiftyFive Tech Projects/my-trello-board/src/assets/loginFrame.jpg";

export const useStyle = makeStyles({
  loginMainContainer: {
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    minHeight: "100vh",
    background: `linear-gradient(rgba(0,0,0,.1),rgba(0,0,0,.1)),url(${LoginBgImage})`,
    backgroundRepeat: "no-repeat",
    backgroundPosition: "center",
    backgroundSize: "cover",
    // width: "100%",
  },
  loginButton: {
    margin: "8px 0px",
    backgroundColor: "#5AAC44",
    color: "#fff",
    "&:hover": {
      backgroundColor: "#63b14d",
    },
  },
  loginContainer: {
    marginTop: 8,
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    padding: "36px 24px",
    borderRadius: 4,
    boxShadow: "rgb(0 0 0 / 10%) 0 0 10px",
    backgroundColor: "#ffffff",
    zIndex: 10,
  },
  loginText: {
    color: "#606060",
  },
  imageContainer: {
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    margin: "36px 0px",
  },
  link: {
    textDecoration: "none",
    color: "blue",
    "&:hover": {
      textDecoration: "underline",
    },
  },
});
