import React, { useEffect } from "react";
import TrelloList from "../TrelloList";
import Header from "../../components/Header";
import TrelloAddButton from "../../components/TrelloAddButton";
import { connect } from "react-redux";
import { DragDropContext, Droppable } from "react-beautiful-dnd";
import { sort } from "../../core/actions";
import { fetchBoardDetailsStart } from "../../core/actions";
import Loader from "../../components/Loader/Loader";
import { useStyle } from "./Styles/styles";
import { useParams } from "react-router-dom";

const TrelloBoard = (props) => {
  const classes = useStyle();
  const { lists, showLoader } = props;
  console.log("Props....  ", props);
  let { id } = useParams();
  id = id * 1;

  useEffect(() => {
    props.fetchBoardDetailsStart({ id });
  }, []);

  const handleDragEnd = (result) => {
    console.log("RESULT IN HANDLE-DRAG-END...", result);
    const { destination, source, draggableId, type } = result;
    if (!destination) {
      return;
    }

    props.sort(destination.droppableId * 1, draggableId * 1);
    //to convert the data passed into Int, it get multiplied by 1

    // props.dispatch(
    //   sort(
    //     source.droppableId,
    //     destination.droppableId,
    //     source.index,
    //     destination.index,
    //     draggableId,
    //     type
    //   )
    // );
  };

  return (
    <>
      {showLoader ? (
        <Loader />
      ) : (
        <div className={classes.app}>
          <Header board = {false} />
          <DragDropContext onDragEnd={handleDragEnd}>
            <div className={classes.container}>
              <Droppable
                droppableId="all-list"
                direction="horizontal"
                type="list"
              >
                {(provided) => (
                  <div
                    className={classes.listDisplay}
                    {...provided.droppableProps}
                    ref={provided.innerRef}
                  >
                    {lists.map((list, index) => (
                      <div key={list.id} className={classes.trelloList}>
                        <TrelloList
                          listId={list.id}
                          title={list.title}
                          cards={list.cards}
                          index={index}
                        />
                      </div>
                    ))}
                    {provided.placeholder}
                    <TrelloAddButton list="list" />
                  </div>
                )}
              </Droppable>
            </div>
          </DragDropContext>
        </div>
      )}
    </>
  );
};

const mapStateToProps = ({ lists, boards }) => ({
  lists: lists.lists,
  showLoader: lists.showLoader,
});

const mapDispatchToProps = (dispatch) => ({
  fetchBoardDetailsStart: ({ id }) => dispatch(fetchBoardDetailsStart({ id })),
  sort: (newListId, cardId) => dispatch(sort(newListId, cardId)),
});

export default connect(mapStateToProps, mapDispatchToProps)(TrelloBoard);
