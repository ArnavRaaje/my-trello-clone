import { makeStyles } from "@material-ui/core";
import beach from "../../../assets/beach.jpg";

export const useStyle = makeStyles({
    app: {
      margin: 0,
      padding: 0,
      minHeight: "100vh",
      background: `url(${beach})`,
      backgroundRepeat: "no-repeat",
      backgroundPosition: "center",
      backgroundSize: "cover",
      overflowY: "hidden",
      overflowX: "scroll",
      width: "100%",
      fontFamily:
        "-apple-system, BlinkMacSystemFont, Segoe UI,Roboto,Noto Sans,Ubuntu,Droid Sans,Helvetica Neue,sans-serif",
    },
    container: {
      marginTop: 50,
    },
    listDisplay: {
      display: "flex",
      flexDirection: "row",
    },
    trelloList: {
      margin: 4,
    },
  });
  