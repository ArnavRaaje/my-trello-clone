import React, { useEffect } from "react";
import { Routes, Route } from "react-router-dom";
import { Navigate } from "react-router-dom";
import AllBoards from "./AllBoards";
import Login from "./Login/Login";
import Signup from "./Signup/Signup";
import TrelloBoard from "./TrelloBoard/TrelloBoard";
import { logInSuccess } from "../core/actions";
import { connect } from "react-redux";

const App = (props) => {
  useEffect(() => {
    if (localStorage.getItem("userInfo")) {
      const user = JSON.parse(localStorage.getItem("userInfo"));
      console.log("user info...", user);
      props.logInSuccess(user);
    }
  }, []);

  const PrivateRoute = ({ children }) => {
    return props.currentUser ? children : <Navigate to="/" />;
  };

  return (
    <div>
      <Routes>
        <Route path="/" element={<Login />} />
        <Route path="signup" element={<Signup />} />
        <Route
          path="boards"
          element={
            <PrivateRoute>
              <AllBoards />
            </PrivateRoute>
          }
        />
        <Route
          path="boards/:id"
          element={
            <PrivateRoute>
              <TrelloBoard />
            </PrivateRoute>
          }
        />
      </Routes>
    </div>
  );
};

const mapStateToProps = ({ user }) => ({
  currentUser: user.currentUser,
});

const mapDispatchToProps = (dispatch) => ({
  logInSuccess: (user) => dispatch(logInSuccess(user)),
});

export default connect(mapStateToProps, mapDispatchToProps)(App);
