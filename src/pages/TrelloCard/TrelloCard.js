import { Card, Button, Icon, Typography, TextField } from "@material-ui/core";
import React, { useState, useEffect } from "react";
import { Draggable } from "react-beautiful-dnd";
import { connect } from "react-redux";
import { editCard, deleteCard } from "../../core/actions";
import Modal from "react-modal";
import VideoLableIcon from "@material-ui/icons/VideoLabel";
import PersonOutlineIcon from "@material-ui/icons/PersonOutline";
import LabelOutlinedIcon from "@material-ui/icons/LabelOutlined";
import PlaylistAddCheckOutlinedIcon from "@material-ui/icons/PlaylistAddCheckOutlined";
import WatchLaterOutlinedIcon from "@material-ui/icons/WatchLaterOutlined";
import AttachFileOutlinedIcon from "@material-ui/icons/AttachFileOutlined";
import WebAssetOutlinedIcon from "@material-ui/icons/WebAssetOutlined";
import { useStyle } from "./Styles/styles";
Modal.setAppElement("#root");

const TrelloCard = (props) => {
  const { cardTitle, id, index, listId, listTitle, cardDescription } = props;
  const classes = useStyle();
  const [isCardEditing, setIsCardEditing] = useState(false);
  const [showModal, setShowModal] = useState(false);
  const [newCardTitle, setNewCardTitle] = useState(cardTitle);
  const [description, setDescription] = useState(cardDescription);
  const [isDescEditing, setIsDescEditing] = useState(false);
  const [isVisible, setIsVisible] = useState("block");
  const [editBtn, setEditBtn] = useState("none");

  const handleChange = (e) => {
    setNewCardTitle(e.target.value);
  };

  const saveCard = (e) => {
    e.preventDefault();
    setShowModal(false);
    if (cardTitle !== newCardTitle || description !== cardDescription) {
      props.editCard({ id, newCardTitle, description });
    }
  };

  const handleDeleteCard = (e) => {
    e.preventDefault();
    props.deleteCard({ id });
  };

  const handleCardTitleEditing = (e) => {
    e.preventDefault();
    setIsCardEditing(false);
  };

  const renderCardTitleEditForm = (cardTitleValue, handleTitleChange) => {
    return (
      <form onSubmit={handleCardTitleEditing}>
        <TextField
          type="text"
          value={cardTitleValue}
          onChange={handleTitleChange}
          autoFocus
          onBlur={handleCardTitleEditing}
        />
      </form>
    );
  };

  useEffect(() => {
    if (description !== "") setIsVisible("block");
    else setIsVisible("none");
  }, []);

  const renderDescriptionEditForm = () => {
    return (
      <div>
        <textarea
          placeholder="Add a more detailed description..."
          autoFocus
          value={description}
          onChange={(e) => setDescription(e.target.value)}
          className={classes.textareaEdit}
        ></textarea>
        <div className={classes.formButton}>
          <Button
            onClick={handleSaveAndVisibility}
            variant="outlined"
            color="success"
          >
            Save
          </Button>
          <Icon onClick={closeDescEditForm}> close</Icon>
        </div>
      </div>
    );
  };

  const handleSaveAndVisibility = () => {
    setIsDescEditing(false);
    if (description === "") {
      setIsVisible("block");
    } else {
      setEditBtn("block");
    }
  };
  const closeDescEditForm = () => {
    setIsDescEditing(false);
    setDescription(description);
    if (description === "") {
      setIsVisible("block");
    } else {
      setEditBtn("block");
    }
  };

  const renderDescription = () => {
    return (
      <div
        onClick={() => setIsDescEditing(true)}
        className={classes.description}
      >
        {description}
      </div>
    );
  };

  const handleDescriptionOnClick = () => {
    setIsDescEditing(true);
    setEditBtn("none");
    setIsVisible("none");
  };

  return (
    <Draggable draggableId={String(id)} index={index}>
      {(provided) => (
        <div
          className={classes.cardContainer}
          ref={provided.innerRef}
          {...provided.draggableProps}
          {...provided.dragHandleProps}
        >
          <Card className={classes.card}>
            <Icon
              onMouseDown={handleDeleteCard}
              className={classes.deleteButton}
              fontSize="small"
            >
              delete
            </Icon>
            <Typography
              color="textSecondary"
              component="p"
              className={classes.cardContent}
              onClick={() => setShowModal(true)}
            >
              {/* {cardTitle} */}
              {newCardTitle}
            </Typography>

            {/* MODAL RENDER */}
            <Modal
              isOpen={showModal}
              onRequestClose={saveCard}
              className={classes.modal}
              style={{
                overlay: {
                  backgroundColor: "rgba(0,0,0,0.5)",
                  border: "none",
                  borderColor: "white",
                },
                content: {
                  color: "black",
                  backgroundColor: "#ebecf0",
                  minHeight: "100vh",
                  overflowY: "scroll",
                },
              }}
            >
              <div className={classes.modalContainer}>
                <div className={classes.modalTopHead}>
                  <div className={classes.titleHead}>
                    <VideoLableIcon className={classes.modalIcon} />
                    {isCardEditing ? (
                      renderCardTitleEditForm(newCardTitle, handleChange)
                    ) : (
                      <h3
                        className={classes.cardTitle}
                        onClick={() => setIsCardEditing(true)}
                      >
                        {newCardTitle}
                      </h3>
                    )}
                  </div>
                  <Icon onClick={saveCard} className={classes.closeIcon}>
                    close
                  </Icon>
                </div>
                <div className={classes.listTitle}>
                  in list{" "}
                  <span style={{ textDecoration: "underline" }}>
                    {listTitle}
                  </span>
                </div>
                <div className={classes.descriptionContainer}>
                  <div className={classes.containerLeft}>
                    <div className={classes.descriptionHead}>
                      <Icon className={classes.modalIcon}>subject</Icon>
                      <h3>Description</h3>
                      <Button
                        variant="outlined"
                        onClick={handleDescriptionOnClick}
                        style={{
                          display: editBtn,
                          fontSize: "small",
                          marginLeft: 8,
                        }}
                      >
                        Edit
                      </Button>
                    </div>
                    <textarea
                      placeholder="Add a more detailed description..."
                      value={description}
                      onChange={(e) => setDescription(e.target.value)}
                      className={classes.textarea}
                      onClick={handleDescriptionOnClick}
                      style={{ display: isVisible }}
                    />
                    {isDescEditing
                      ? renderDescriptionEditForm()
                      : renderDescription()}
                    <div className={classes.modalTopHead}>
                      <div className={classes.titleHead}>
                        <Icon className={classes.modalIcon}>listalt </Icon>
                        <h3>Activity</h3>
                      </div>
                      <Button
                        color="success"
                        variant="outlined"
                        className={classes.showDetailsBtn}
                      >
                        Show Details
                      </Button>
                    </div>
                  </div>
                  <div className={classes.containerRight}>
                    <div className={classes.suggestedDiv}>
                      <p>Suggested</p>
                      <Icon fontSize="small">settings</Icon>
                    </div>
                    <div className={classes.sideBarTabs}>
                      <PersonOutlineIcon
                        fontSize="small"
                        className={classes.sBTIcons}
                      />
                      Join
                    </div>
                    <p>Add to card</p>
                    <div className={classes.sideBarTabs}>
                      <PersonOutlineIcon
                        fontSize="small"
                        className={classes.sBTIcons}
                      />
                      Members
                    </div>
                    <div className={classes.sideBarTabs}>
                      <LabelOutlinedIcon
                        fontSize="small"
                        className={classes.sBTIcons}
                      />
                      Labels
                    </div>
                    <div className={classes.sideBarTabs}>
                      <PlaylistAddCheckOutlinedIcon
                        fontSize="small"
                        className={classes.sBTIcons}
                      />
                      Checklist
                    </div>
                    <div className={classes.sideBarTabs}>
                      <WatchLaterOutlinedIcon
                        fontSize="small"
                        className={classes.sBTIcons}
                      />
                      Dates
                    </div>
                    <div className={classes.sideBarTabs}>
                      <AttachFileOutlinedIcon
                        fontSize="small"
                        className={classes.sBTIcons}
                      />
                      Attachment
                    </div>
                    <div className={classes.sideBarTabs}>
                      <WebAssetOutlinedIcon
                        fontSize="small"
                        className={classes.sBTIcons}
                      />
                      Cover
                    </div>
                  </div>
                </div>
              </div>
            </Modal>
          </Card>
        </div>
      )}
    </Draggable>
  );
};

const mapDispatchToProps = (dispatch) => ({
  editCard: ({ id, newCardTitle, description }) =>
    dispatch(editCard({ id, newCardTitle, description })),
  deleteCard: ({ id }) => dispatch(deleteCard({ id })),
});

export default connect(null, mapDispatchToProps)(TrelloCard);
