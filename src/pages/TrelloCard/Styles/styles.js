import { makeStyles } from "@material-ui/styles";

export const useStyle = makeStyles((theme) => ({
  cardContainer: {
    marginTop: 8,
    position: "relative",
  },

  card: {
    "&:hover": {
      "& .MuiIcon-root": {
        display: "block",
      },
    },
  },

  deleteButton: {
    position: "absolute",
    top: 8,
    right: 5,
    opacity: 0.4,
    display: "none",
    transition: "opacity 0.3s ease-in-out",
    cursor: "pointer",
    "&:hover": {
      opacity: 1,
    },
  },
  cardContent: {
    overflowWrap: "break-word",
    paddingTop: 8,
    paddingBottom: 8,
    paddingRight: 24,
    paddingLeft: 8,
  },

  //   modal start
  modal: {
    position: "absolute",
    top: 60,
    left: 200,
    right: 200,
    bottom: 40,
    border: "none",
  },

  modalContainer: {
    display: "flex",
    flexDirection: "column",
    padding: 16,
    marginBottom: 100,
  },
  modalTopHead: {
    display: "flex",
    alignItems: "center",
    flexGrow: 1,
    justifyContent: "space-between",
  },
  titleHead: {
    display: "flex",
    alignItems: "center",
  },
  descriptionHead: {
    display: "flex",
    alignItems: "center",
  },
  modalIcon: {
    color: "grey",
    marginRight: 12,
  },
  listTitle: {
    marginLeft: 36,
    marginBottom: 15,
  },
  cardTitle: {
    marginBottom: 0,
    marginTop: 0,
  },
  closeIcon: {
    cursor: "pointer",
    backgroundColor: "inherit",
    width: 24,
    height: 24,
    borderRadius: "50%",
    "&: hover": {
      "& .makeStyles-closeIcon-31 ": {
        backgroundColor: "#d3d3d3f5",
      },
    },
  },
  descriptionContainer: {
    display: "flex",
    flexDirection: "row",
    flexWrap:"wrap",
    justifyContent: "space-between",
  },
  containerLeft: {
    width: "70%",
    margin: 8,
  },
  textarea: {
    width: "90%",
    height: 50,
    padding: 8,
    border: "none",
    marginLeft: 36,
    resize: "none",
    borderRadius:4
  },
  textareaEdit: {
    width: "90%",
    height: 90,
    padding: 8,
    border: "none",
    marginLeft: 36,
    marginBottom: 8,
    resize: "none",
    borderRadius:4
  },
  showDetailsBtn: {
    marginRight: 8,
  },

  //   right container
  containerRight: {
    display: "flex",
    flexDirection: "column",
    width: "20%",
  },
  suggestedDiv: {
    display: "flex",
    justifyContent: "space-between",
    alignItems: "center",
    margin: 8,
  },
  sideBarTabs: {
    display: "flex",
    alignItems: "center",
    backgroundColor: "#fff",
    margin: 4,
    padding: 8,
    borderRadius:3,
    cursor:"pointer",
  },
  sBTIcons: {
    marginRight: 4,
  },

//   edit form
  description: {
    marginLeft: 36,
    cursor:"pointer"
  },
  formButton: {
    display: "flex",
    alignItems: "center",
    marginLeft: 36,
    cursor: "pointer",
  },
}));
