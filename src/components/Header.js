import React, { useState, useEffect } from "react";
import {
  Avatar,
  Container,
  makeStyles,
  Typography,
  Menu,
  MenuItem,
  Divider,
} from "@material-ui/core";
import { Icon } from "@material-ui/core";
import { deepPurple } from "@material-ui/core/colors";
import { connect } from "react-redux";
import { useNavigate } from "react-router";
import { logOutStart } from "../core/actions";
import AvatarInitials from "../components/Avatar/index";

const useStyles = makeStyles({
  header: {
    height: "9vh",
    display: "flex",
    position: "fixed",
    maxWidth: "100vw",
    alignItems: "center",
    justifyContent: "space-between",
    color: "#fff",
  },
  arello: {
    display: "flex",
    alignItems: "center",
    cursor: "pointer",
  },
  logoutIcon: {
    transition: "backgroundColor 0.3s ease-in-out",
    cursor: "pointer",
    "& :hover": {
      backgroundColor: "yellow",
    },
  },
  avatar: {
    backgroundColor: deepPurple[500],
    cursor: "pointer",
  },
  menu: {
    marginTop: 40,
  },
  menuContainer: {
    width: 300,
    display: "flex",
    flexDirection: "column",
    "& .MuiList-root": {
      padding: 16,
    },
  },
  menuHeader: {
    display: "flex",
    alignItems: "center",
  },

  account: {
    width: "85%",
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
  },
  userDetailContainer: {
    display: "flex",
    alignItems: "center",
  },
  usernameContainer: {
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
  },
});

const Header = (props) => {
  const classes = useStyles();
  const navigate = useNavigate();
  const { currentUser } = props;
  const { board } = props;
  const userName = `${currentUser.firstName} ${currentUser.lastName}`;
  const userEmail = currentUser.email;

  const bgColor = board ? "#0079BF" : "rgba(60, 60, 60, 0.60)";
  const [anchorEl, setAnchorEl] = React.useState(null);
  const open = Boolean(anchorEl);
  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };
  const handleClose = () => {
    setAnchorEl(null);
  };

  const handleLogout = (e) => {
    e.preventDefault();
    props.logOutStart();
    navigate("/");
  };

  return (
    <Container
      maxWidth="xl"
      className={classes.header}
      style={{ backgroundColor: bgColor }}
    >
      <Typography
        variant="h6"
        component="h2"
        gutterBottom
        className={classes.arello}
      >
        <Icon>appsicon</Icon>
        Trello
      </Typography>

      <Avatar onClick={handleClick} className={classes.avatar}>
        <AvatarInitials userName={userName} />
      </Avatar>
      <Menu
        anchorEl={anchorEl}
        id="account-menu"
        open={open}
        onClose={handleClose}
        transformOrigin={{ horizontal: "right", vertical: "top" }}
        anchorOrigin={{ horizontal: "right", vertical: "bottom" }}
        className={classes.menu}
      >
        <div className={classes.menuContainer}>
          <div className={classes.menuHeader}>
            <div className={classes.account}>
              <p>Account </p>
            </div>
            <Icon onClick={handleClose} style={{ cursor: "pointer" }}>
              close
            </Icon>
          </div>
          <Divider />
          <div>
            <div className={classes.userDetailContainer}>
              <Avatar onClick={handleClick} className={classes.avatar}>
                <AvatarInitials userName={userName} />
              </Avatar>
              <div className={classes.usernameContainer}>
                <p>{userName}</p>
                <p>{userEmail}</p>
              </div>
            </div>
          </div>
          <Divider />
          <MenuItem></MenuItem>

          <MenuItem title="Log out" onClick={handleLogout}>
            <Icon>logout</Icon>
            Logout
          </MenuItem>
        </div>
      </Menu>
    </Container>
  );
};

const mapStateToProps = ({ user }) => ({
  currentUser: user.currentUser,
});

const mapDispatchToProps = (dispatch) => ({
  logOutStart: () => dispatch(logOutStart()),
});

export default connect(mapStateToProps, mapDispatchToProps)(Header);
