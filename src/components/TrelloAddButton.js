import { Icon, Typography, Box, TextField, Button } from "@material-ui/core";
import { makeStyles } from "@material-ui/styles";
import React, { useState } from "react";
import { connect } from "react-redux";
import { addList, addCard } from "../core/actions";

const useStyle = makeStyles({
  buttonContainer: {
    display: "flex",
    alignItems: "center",
    cursor: "pointer",
    marginTop: 12,
    borderRadius: 4,
    height: 32,
    minWidth: 265,
    transition: "backgroundColor 0.5s ease-in-out",
    backgroundColor: "rgb(230, 230, 230)",
    "&:hover": {
      backgroundColor: "#d3d3d3f5",
    },
  },

  boxContainer: {
    backgroundColor: "rgb(230, 230, 230)",
    borderRadius: 4,
    minWidth: 265,
  },

  textfield: {
    paddingBottom: 8,
    minWidth: "100%",
    backgroundColor: "#fff",
    border: 2,
    borderColor: "black",
    borderRadius: 4,
    overflow: "hidden",
  },

  saveBtnContainer: {
    display: "flex",
    marginLeft: 0,
    alignItems: "center",
  },
  btn: {
    marginTop: 8,
  },
});

const TrelloAddButton = (props) => {
  console.log("props received...", props);
  const { list, listId, boardId } = props;
  const classes = useStyle();
  const [showForm, setShowForm] = useState(false);
  const [input, setInput] = useState("");

  // ____________________INPUT DISPATCH -- UPDATE FUNCTIONS HERE______________________
  const handleAddList = () => {
    if (input) {
      props.addList({ input, boardId });
    }
    setInput("");
  };

  const handleAddCard = () => {
    console.log("ID in add card... ", listId);
    if (input) {
      props.addCard({ listId, input });
    }
    setInput("");
  };

  //_______________________FORM RENDER FUNCTION HERE________________
  const renderForm = () => {
    const placeholder = list
      ? "Enter list title..."
      : "Enter a title for this card...";
    const buttonTitle = list ? "Add List" : "Add Card";
    const topMargin = list ? 12 : 0;
    const leftMargin = list ? 4 : 0;
    const padAll = list ? 8 : 0;
    const length = list ? "fit-content" : "auto";
    const inputfieldMargin = list ? 0 : 8
    return (
      <Box
        component="form"
        autoComplete="off"
        className={classes.boxContainer}
        style={{
          marginTop: topMargin,
          marginLeft: leftMargin,
          padding: padAll,
          height: length,
        }}
      >
        <TextField
          style={{marginTop: inputfieldMargin}}
          className={classes.textfield}
          id="outlined-multiline-flexible"
          label={placeholder}
          multiline
          maxRows={4}
          autoFocus
          onBlur={() => setShowForm(false)}
          value={input}
          onChange={(e) => setInput(e.target.value)}
        />
        <div className={classes.saveBtnContainer}>
          <Button
            variant="contained"
            color="primary"
            className={classes.btn}
            onMouseDown={list ? handleAddList : handleAddCard}
          >
            {buttonTitle}
          </Button>

          <Icon
            onClick={() => setShowForm(false)}
            style={{ cursor: "pointer", marginLeft: 8, marginTop: 5 }}
          >
            close
          </Icon>
        </div>
      </Box>
    );
  };

  //_______________________BUTTON RENDER FUNCTION HERE__________________
  const renderAddButton = () => {
    const buttonText = list ? "Add another list " : "Add a card";
    const leftMargin = list ? 4 : 0;
    const rightMargin = list ? 16 : 0;

    return (
      <Typography
        style={{ marginLeft: leftMargin, marginRight:rightMargin }}
        component="span"
        onClick={() => setShowForm(true)}
        className={classes.buttonContainer}
      >
        <Icon>add</Icon>
        <p>{buttonText}</p>
      </Typography>
    );
  };

  return showForm ? renderForm() : renderAddButton();
};

const mapStateToProps = ({ lists }) => ({
  boardId: lists.boardId,
});

const mapDispatchToProps = (dispatch) => ({
  addList: ({ input, boardId }) => dispatch(addList({ input, boardId })),
  addCard: ({ listId, input }) => dispatch(addCard({ listId, input })),
});
export default connect(mapStateToProps, mapDispatchToProps)(TrelloAddButton);
