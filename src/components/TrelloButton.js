import { Button, makeStyles } from "@material-ui/core";
import React from "react";

const useStyle = makeStyles({
  button: {
    backgroundColor: "primary"
  },
});

const TrelloButton = ({ children, onClick }) => {
  const classes = useStyle();
  return (
    <Button
      variant="contained"
      onMouseDown={onClick}
      // className={classes.button}
      color="success"
    >
      {children}
    </Button>
  );
};

export default TrelloButton;
