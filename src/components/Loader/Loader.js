import React from "react";
import { CircularProgress, makeStyles } from "@material-ui/core";
import beach from "../../assets/beach.jpg";

const useStyle = makeStyles({
  loader: {
    display: "flex",
    width: "100%",
    height: "100vh",
    justifyContent: "center",
    alignItems: "center",
    background: `linear-gradient(rgba(0,0,0,.6),rgba(0,0,0,.6)), url(${beach})`,
    backgroundRepeat: "no-repeat",
    backgroundPosition: "center",
    backgroundSize: "cover",
  },
});

const Loader = () => {
  const classes = useStyle();
  return (
    <div className={classes.loader}>
      <CircularProgress color="secondary" />
    </div>
  );
};

export default Loader;
