import React from "react";

const AvatarInitials = ({ userName }) => {
  const getInitials = (userName) => {
    const fullName = userName.split(" ");
    const initials = fullName.shift().charAt(0) + fullName.pop().charAt(0);
    return initials.toUpperCase();
  };
  return <div>{getInitials(userName)}</div>;
};

export default AvatarInitials;
