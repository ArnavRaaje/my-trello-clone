import { Card, Icon, makeStyles, TextField } from "@material-ui/core";
import React from "react";

const useStyle = makeStyles({
  formContainer: {
    width: 280,
  },
  formCard: {
    padding: 8,
    marginTop: 8,
    marginBottom: 8,
  },
  formButton: {
    display: "flex",
    alignItems: "center",
  },
  formText: {
    width: "100%",
  },
});

const TrelloForm = ({ list, title, onChange, closeForm, children }) => {
  const classes = useStyle();
  const placeholder = list ? "Enter List title..." : "Enter card title...";
  const handleFocus = (e) => {
    e.target.select();
  };
  return (
    <div className={classes.formContainer}>
      <Card className={classes.formCard}>
        <TextField
          placeholder={placeholder}
          autoFocus
          onFocus={handleFocus}
          value={title}
          onChange={(e) => onChange(e)}
          onBlur={closeForm}
          className={classes.formText}
        />
      </Card>
      <div className={classes.formButton}>
        {children}
        <Icon onMouseDown={closeForm}> close</Icon>
      </div>
    </div>
  );
};

export default TrelloForm;
