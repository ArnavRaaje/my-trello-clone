import { gql } from "@apollo/client";
import { client } from "../../../index";

const loginUserMutation = gql`
  mutation Mutation($email: String!, $password: String!) {
    login(email: $email, password: $password) {
      token
      user {
        id
        firstName
        lastName
        email
      }
    }
  }
`;

const createUserMutation = gql`
  mutation Signup(
    $email: String!
    $password: String!
    $firstName: String!
    $lastName: String!
  ) {
    signup(
      email: $email
      password: $password
      firstName: $firstName
      lastName: $lastName
    ) {
      token
      user {
        firstName
        lastName
        email
        id
      }
    }
  }
`;
export const fetchUserDataAfterLogIn = (email, password) => {
  return client.mutate({
    mutation: loginUserMutation,
    variables: {
      email: email,
      password: password,
    },
  });
};

export const createUserAfterSignUp = (firstName, lastName, email, password) => {
  return client.mutate({
    mutation: createUserMutation,
    variables: {
      firstName: firstName,
      lastName: lastName,
      email: email,
      password: password,
    },
  });
};
