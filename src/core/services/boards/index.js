import { gql } from "@apollo/client";
import { client } from "../../../index";

const getAllDataquery = gql`
  query GetBoardDetails($id: Int!) {
    getBoardDetails(id: $id) {
      id
      title
      lists {
        id
        title
        cards {
          id
          title
          description
        }
      }
    }
  }
`;

const getAllBoardsQuery = gql`
  query Query {
    getAllBoards {
      id
      title
      lists {
        id
        title
        cards {
          id
          title
          description
        }
      }
    }
  }
`;

const addNewBoardMutation = gql`
  mutation CreateBoard($newBoardTitle: String!) {
    createBoard(title: $newBoardTitle) {
      id
      title
      lists {
        id
        title
        cards {
          id
          title
          description
        }
      }
    }
  }
`;

const addNewListMutation = gql`
  mutation CreateList($title: String!, $boardId: Int!) {
    createList(title: $title, boardId: $boardId) {
      id
      title
      lists {
        id
        title
        cards {
          id
          title
          description
        }
      }
    }
  }
`;

const addNewCardMutation = gql`
  mutation CreateCardofList($title: String!, $listId: Int!) {
    createCardofList(title: $title, listId: $listId) {
      id
      title
      lists {
        id
        title
        cards {
          title
          id
          description
        }
      }
    }
  }
`;

const deleteListMutation = gql`
  mutation CreateList($listId: Int!) {
    deleteListById(id: $listId) {
      id
      title
      lists {
        id
        title
        cards {
          id
          title
          description
        }
      }
    }
  }
`;

const deleteCardMutation = gql`
  mutation Mutation($id: Int!) {
    deleteCardById(id: $id) {
      id
      title
      lists {
        id
        title
        cards {
          id
          title
          description
        }
      }
    }
  }
`;

const editListTitleMutation = gql`
  mutation CreateCardofList($listId: Int!, $listTitle: String!) {
    updateListDetails(id: $listId, title: $listTitle) {
      id
      title
      lists {
        id
        title
        cards {
          title
          id
          description
        }
      }
    }
  }
`;

const editCardMutation = gql`
  mutation Mutation($id: Int!, $title: String, $description: String) {
    updateCardDetails(id: $id, title: $title, description: $description) {
      id
      title
      lists {
        id
        title
        cards {
          id
          title
          description
        }
      }
    }
  }
`;

const dragMutation = gql`
  mutation Mutation($cardId: Int!, $newListId: Int!) {
    dragCard(id: $cardId, listId: $newListId) {
      id
      title
      lists {
        id
        title
        cards {
          title
          id
        }
      }
    }
  }
`;

export const getAllBoardsFromDb = async () => {
  return await client.query({
    query: getAllBoardsQuery,
  });
};

export const getAllDataFromDb = async (id) => {
  return await client.query({
    query: getAllDataquery,
    variables: {
      id: id,
    },
  });
};

export const addnewBoardToDb = async (newBoardTitle) => {
  return await client.mutate({
    mutation: addNewBoardMutation,
    variables: {
      newBoardTitle: newBoardTitle,
    },
  });
};

export const addNewListToDb = async (title, boardId) => {
  return await client.mutate({
    mutation: addNewListMutation,
    variables: {
      title: title,
      boardId: boardId,
    },
  });
};

export const addNewCardToDb = async (listId, title) => {
  return await client.mutate({
    mutation: addNewCardMutation,
    variables: {
      listId: listId,
      title: title,
    },
  });
};

export const deleteListFromDb = async (listId) => {
  return await client.mutate({
    mutation: deleteListMutation,
    variables: {
      listId: listId,
    },
  });
};

export const deleteCardFromDb = async (id) => {
  return await client.mutate({
    mutation: deleteCardMutation,
    variables: {
      id: id,
    },
  });
};

export const editListIntoDb = async (listId, listTitle) => {
  return await client.mutate({
    mutation: editListTitleMutation,
    variables: {
      listId: listId,
      listTitle: listTitle,
    },
  });
};

export const editCardIntoDb = async (id, newCardTitle, description) => {
  return await client.mutate({
    mutation: editCardMutation,
    variables: {
      id: id,
      title: newCardTitle,
      description: description,
    },
  });
};

export const dragUpdateIntoDb = async (newListId, cardId) => {
  return await client.mutate({
    mutation: dragMutation,
    variables: {
      newListId: newListId,
      cardId: cardId,
    },
  });
};
