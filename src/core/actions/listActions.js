import { CONSTANTS } from ".";

export const fetchBoardDetailsStart = ({ id }) => ({
  type: CONSTANTS.FETCH_BOARD_DETAILS,
  payload: { id },
});

export const fetchAllBoardsSuccess = (data) => ({
  type: CONSTANTS.FETCH_BOARD_DETAILS_SUCCESS,
  payload: data,
});

export const addList = ({ input, boardId }) => ({
  type: CONSTANTS.ADD_LIST,
  payload: { input, boardId },
});

export const addListSuccess = (data) => ({
  type: CONSTANTS.ADD_LIST_SUCCESS,
  payload: data,
});

export const deleteList = ({ listId }) => ({
  type: CONSTANTS.DELETE_LIST_INIT,
  payload: { listId },
});

export const deleteListSuccess = (lists) => ({
  type: CONSTANTS.DELETE_LIST_SUCCESS,
  payload: lists,
});

export const editListTitle = ({ listId, listTitle }) => ({
  type: CONSTANTS.EDIT_LIST_TITLE,
  payload: { listId, listTitle },
});

export const editListTitleSuccess = (lists) => ({
  type: CONSTANTS.EDIT_LIST_TITLE_SUCCESS,
  payload: lists,
});

export const sort = (newListId, cardId) => ({
  type: CONSTANTS.DRAG_INITIALIZE,
  payload: { newListId, cardId },
});

export const dragSuccess = (boardDetails) => ({
  type: CONSTANTS.DRAG_SUCCESSFUL,
  payload: boardDetails,
});
