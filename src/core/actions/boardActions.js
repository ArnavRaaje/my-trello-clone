import { CONSTANTS } from ".";

export const getAllBoardStart = () => ({
  type: CONSTANTS.GET_ALL_BOARDS_INIT,
});

export const getAllBoardsSuccess = (boards) => ({
  type: CONSTANTS.GET_ALL_BOARDS_SUCCESS,
  payload: boards,
});

export const addBoard = ({ newBoardTitle }) => ({
  type: CONSTANTS.ADD_BOARD_INIT,
  payload: { newBoardTitle },
});

export const addBoardSuccess = (boards) => ({
  type: CONSTANTS.ADD_BOARD_SUCCESS,
  payload: boards,
});
