import { CONSTANTS } from ".";

export const emailSignUpStart = ({ firstName, lastName, email, password }) => ({
  type: CONSTANTS.EMAIL_SIGN_UP_START,
  payload: { firstName, lastName, email, password },
});

export const signUpSuccess = (user) => ({
  type: CONSTANTS.SIGN_UP_SUCCESS,
  payload: user,
});

export const emailSignInStart = ({ email, password }) => ({
  type: CONSTANTS.EMAIL_SIGN_IN_START,
  payload: { email, password },
});

export const logInSuccess = (user) => ({
  type: CONSTANTS.LOG_IN_SUCCESS,
  payload: user,
});

export const logOutStart = () => ({
  type: CONSTANTS.LOG_OUT_START,
});

export const logOutSuccess = () => ({
  type: CONSTANTS.LOG_OUT_SUCCESS,
});

export const userError = (err) => ({
  type: CONSTANTS.USER_ERROR,
  payload: err,
});
