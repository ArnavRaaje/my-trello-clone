import { CONSTANTS } from ".";

export const addCard = ({ listId, input }) => ({
  type: CONSTANTS.ADD_CARD,
  payload: { listId, input },
});

export const addCardSuccess = (lists) => ({
  type: CONSTANTS.ADD_CARD_SUCCESS,
  payload: lists,
});

export const editCard = ({ id, newCardTitle, description }) => ({
  type: CONSTANTS.EDIT_CARD_INIT,
  payload: { id, newCardTitle, description },
});

export const editCardSuccess = (lists) => ({
  type: CONSTANTS.EDIT_CARD_SUCCESS,
  payload: lists,
});

export const deleteCard = ({ id }) => ({
  type: CONSTANTS.DELETE_CARD_INIT,
  payload: { id },
});

export const deleteCardSuccess = (lists) => ({
  type: CONSTANTS.DELETE_CARD_SUCCESS,
  payload: lists,
});
