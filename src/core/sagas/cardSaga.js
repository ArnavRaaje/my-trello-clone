import {
  CONSTANTS,
  addCardSuccess,
  deleteCardSuccess,
  editCardSuccess,
} from "../actions";

import {
  addNewCardToDb,
  deleteCardFromDb,
  editCardIntoDb,
} from "../services/boards";
import { all, call, takeLatest, put, select } from "@redux-saga/core/effects";

// adding new card...
export function* addNewCard({ payload: { listId, input } }) {
  let response = yield addNewCardToDb(listId, input);
  console.log("response-addCard", response.data.createCardofList[0]);
  yield put(addCardSuccess(response.data.createCardofList[0]));
}

export function* onAddNewCardStart() {
  yield takeLatest(CONSTANTS.ADD_CARD, addNewCard);
}

// DELETING A CARD
export function* startDeleteCard({ payload: { id } }) {
  let response = yield deleteCardFromDb(id);
  console.log("response--deleteCard", response.data.deleteCardById[0]);
  yield put(deleteCardSuccess(response.data.deleteCardById[0]));
}

export function* onDeleteCardStart() {
  yield takeLatest(CONSTANTS.DELETE_CARD_INIT, startDeleteCard);
}

// EDITING A CARD
export function* startEditCard({ payload: { id, newCardTitle, description } }) {
  let response = yield editCardIntoDb(id, newCardTitle, description);
  console.log("response--editCard", response.data.updateCardDetails[0]);
  yield put(editCardSuccess(response.data.updateCardDetails[0]));
}
export function* onEditCardStart() {
  yield takeLatest(CONSTANTS.EDIT_CARD_INIT, startEditCard);
}

// exporting all the sagas...
export default function* cardSaga() {
  yield all([
    call(onAddNewCardStart),
    call(onDeleteCardStart),
    call(onEditCardStart),
  ]);
}
