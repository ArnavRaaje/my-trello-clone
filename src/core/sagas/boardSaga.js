import {
  CONSTANTS,
  fetchAllBoardsSuccess,
  addBoardSuccess,
  dragSuccess,
  getAllBoardsSuccess,
} from "../actions";

import {
  getAllBoardsFromDb,
  getAllDataFromDb,
  addnewBoardToDb,
  dragUpdateIntoDb,
} from "../services/boards";
import { all, call, takeLatest, put, select } from "@redux-saga/core/effects";

// fetching all boards
export function* getAllBoards() {
  try {
    let response = yield getAllBoardsFromDb();
    console.log("get all boards...", response.data.getAllBoards);
    yield put(getAllBoardsSuccess(response.data.getAllBoards));
  } catch (err) {
    console.log(err);
  }
}

export function* onGetAllBoardsStart() {
  yield takeLatest(CONSTANTS.GET_ALL_BOARDS_INIT, getAllBoards);
}

// fetching all the board data on login...
export function* fetchBoardsDetails({ payload: { id } }) {
  try {
    let response = yield getAllDataFromDb(id);
    let board = response.data.getBoardDetails[0];
    console.log("RESPONSE--FETCH-BOARDS...", response);
    yield put(fetchAllBoardsSuccess(board));
  } catch (err) {
    console.log(err);
  }
}

export function* onFetchBoardDetailsStart() {
  yield takeLatest(CONSTANTS.FETCH_BOARD_DETAILS, fetchBoardsDetails);
}

// adding a new BOARD...
export function* addNewBoard({ payload: { newBoardTitle } }) {
  let response = yield addnewBoardToDb(newBoardTitle);
  console.log("response add board...", response.data.createBoard);
  yield put(addBoardSuccess(response));
}

export function* onAddNewBoardStart() {
  yield takeLatest(CONSTANTS.ADD_BOARD_INIT, addNewBoard);
}

// DRAG HAPPENED...
export function* dragStart({ payload: { newListId, cardId } }) {
  let response = yield dragUpdateIntoDb(newListId, cardId);
  yield put(dragSuccess(response.data.dragCard[0]));
}

export function* onDragStart() {
  yield takeLatest(CONSTANTS.DRAG_INITIALIZE, dragStart);
}

// exporting all the sagas...
export default function* boardSaga() {
  yield all([
    call(onFetchBoardDetailsStart),
    call(onDragStart),
    call(onGetAllBoardsStart),
    call(onAddNewBoardStart),
  ]);
}
