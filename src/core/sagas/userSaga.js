import {
  CONSTANTS,
  logInSuccess,
  logOutSuccess,
  signUpSuccess,
} from "../actions";
import { all, call, takeLatest, put } from "@redux-saga/core/effects";
import {
  fetchUserDataAfterLogIn,
  createUserAfterSignUp,
} from "../services/auth";

// LOG IN
export function* emailSignIn({ payload: { email, password } }) {
  try {
    let response = yield fetchUserDataAfterLogIn(email, password);
    let token = response.data.login.token;
    let username = `${response.data.login.user.firstName} ${response.data.login.user.lastName}`;
    let userInfo = response.data.login.user;
    localStorage.setItem("username", username);
    localStorage.setItem("userInfo", JSON.stringify(userInfo));
    localStorage.setItem("token", token);
    yield put(logInSuccess(userInfo));
  } catch (err) {
    console.error(err);
  }
}

export function* onEmailSignInStart() {
  yield takeLatest(CONSTANTS.EMAIL_SIGN_IN_START, emailSignIn);
}

// LOG OUT
export function* logOutStart() {
  try {
    localStorage.removeItem("token");
    localStorage.removeItem("userInfo");
    yield put(logOutSuccess());
  } catch (err) {
    console.log("LogOut error", err);
  }
}

export function* onLogOutStart() {
  yield takeLatest(CONSTANTS.LOG_OUT_START, logOutStart);
}

// SIGN UP
export function* emailSignUp({
  payload: { firstName, lastName, email, password },
}) {
  try {
    let response = yield createUserAfterSignUp(
      firstName,
      lastName,
      email,
      password
    );
    console.log("user-sign-up...", response.data.signup);
    yield put(signUpSuccess(response.data.signup));
  } catch (err) {
    console.error(err);
  }
}

export function* onEmailSignUpStart() {
  yield takeLatest(CONSTANTS.EMAIL_SIGN_UP_START, emailSignUp);
}

export default function* userSagas() {
  yield all([
    call(onEmailSignInStart),
    call(onEmailSignUpStart),
    call(onLogOutStart),
  ]);
}
