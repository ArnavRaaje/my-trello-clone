import { all, call } from "redux-saga/effects";
import boardSaga from "./boardSaga";
import cardSaga from "./cardSaga";
import listSaga from "./listSaga";
import userSagas from "./userSaga";

export default function* rootSaga() {
  yield all([call(userSagas), call(boardSaga), call(listSaga), call(cardSaga)]);
}
