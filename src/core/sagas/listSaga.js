import {
  CONSTANTS,
  addListSuccess,
  deleteListSuccess,
  editListTitleSuccess,
} from "../actions";

import {
  addNewListToDb,
  deleteListFromDb,
  editListIntoDb,
} from "../services/boards";
import { all, call, takeLatest, put, select } from "@redux-saga/core/effects";

// adding new list...
export function* addNewList({ payload: { input, boardId } }) {
  let response = yield addNewListToDb(input, boardId);
  console.log("response-addList", response);
  let bId = yield select((state) => state.lists.boardId);
  console.log("board id", bId);
  const getBoard = response.data.createList.find(({ id }) => bId === id);
  yield put(addListSuccess(getBoard));
}

export function* onAddNewListStart() {
  yield takeLatest(CONSTANTS.ADD_LIST, addNewList);
}

// DELETING A LIST
export function* startDeleteList({ payload: { listId } }) {
  let response = yield deleteListFromDb(listId);
  console.log("response-deleteLIST", response.data.deleteListById[0]);
  yield put(deleteListSuccess(response.data.deleteListById[0]));
}

export function* onDeleteListStart() {
  yield takeLatest(CONSTANTS.DELETE_LIST_INIT, startDeleteList);
}

// EDITING A LIST
export function* startEditList({ payload: { listId, listTitle } }) {
  let response = yield editListIntoDb(listId, listTitle);
  console.log("response--EditList", response.data.updateListDetails[0]);
  yield put(editListTitleSuccess(response.data.updateListDetails[0]));
}

export function* onEditListStart() {
  yield takeLatest(CONSTANTS.EDIT_LIST_TITLE, startEditList);
}

// exporting all the sagas...
export default function* listSaga() {
  yield all([
    call(onAddNewListStart),
    call(onDeleteListStart),
    call(onEditListStart),
  ]);
}
