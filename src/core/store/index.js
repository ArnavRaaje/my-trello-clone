import { createStore, applyMiddleware } from "redux";
import rootReducer from "../reducers";
import rootSaga from "../sagas/rootSaga";
import createSagaMiddle from "redux-saga";
import { composeWithDevTools } from "redux-devtools-extension";
import logger from "redux-logger";

const sagaMiddleware = createSagaMiddle();
export const middlewares = [sagaMiddleware, logger];

const store = createStore(
  rootReducer,
  composeWithDevTools(applyMiddleware(...middlewares))
);
sagaMiddleware.run(rootSaga);
export default store;
