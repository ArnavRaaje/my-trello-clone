import { CONSTANTS } from "../actions";

const initialState = {
  boards: [],
  showLoader: false,
};

const boardReducer = (state = initialState, action) => {
  switch (action.type) {
    case CONSTANTS.GET_ALL_BOARDS_SUCCESS:{
      return {...state, boards:action.payload}
    }

    case CONSTANTS.ADD_BOARD_INIT: {
      return { ...state, showLoader: true };
    }
    case CONSTANTS.ADD_BOARD_SUCCESS: {
      return { ...state, showLoader: false, boards: action.payload };
    }
    default:
      return state;
  }
};

export default boardReducer;
