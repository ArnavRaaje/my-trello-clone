import { CONSTANTS } from "../actions";

const initialState = {
  lists: [],
  showLoader: false,
  boardId: null,
};

const listReducer = (state = initialState, action) => {
  switch (action.type) {
    case CONSTANTS.FETCH_BOARD_DETAILS: {
      return { ...state, showLoader: true };
    }

    case CONSTANTS.FETCH_BOARD_DETAILS_SUCCESS: {
      return {
        ...state,
        lists: action.payload.lists,
        boardId: action.payload.id,
        showLoader: false,
      };
    }

    case CONSTANTS.ADD_LIST: {
      return { ...state, showLoader: true };
    }

    case CONSTANTS.ADD_LIST_SUCCESS: {
      return { ...state, lists: action.payload.lists, showLoader: false };
    }

    case CONSTANTS.ADD_CARD: {
      return { ...state, showLoader: true };
    }

    case CONSTANTS.ADD_CARD_SUCCESS: {
      const updatedLists = action.payload.lists;
      console.log("UPDATEDLIST IN ADD CARD SUCCESS...", updatedLists);
      return { ...state, lists: updatedLists, showLoader: false };
    }

    case CONSTANTS.EDIT_CARD_INIT: {
      return { ...state, showLoader: true };
    }

    case CONSTANTS.EDIT_CARD_SUCCESS: {
      const updatedLists = action.payload.lists;
      console.log("UPDATEDLIST IN EDIT CARD SUCCESS...", updatedLists);
      return { ...state, lists: updatedLists, showLoader: false };
    }

    case CONSTANTS.DELETE_CARD_INIT: {
      return { ...state, showLoader: true };
    }

    case CONSTANTS.DELETE_CARD_SUCCESS: {
      const updatedLists = action.payload.lists;
      console.log("UPDATEDLIST IN DELETE CARD SUCCESS...", updatedLists);
      return { ...state, lists: updatedLists, showLoader: false };
    }

    case CONSTANTS.EDIT_LIST_TITLE: {
      return { ...state, showLoader: true };
    }

    case CONSTANTS.EDIT_LIST_TITLE_SUCCESS: {
      const updatedLists = action.payload.lists;
      console.log("UPDATEDLIST IN EDIT LIST SUCCESS...", updatedLists);
      return { ...state, lists: updatedLists, showLoader: false };
    }

    case CONSTANTS.DELETE_LIST_INIT: {
      return { ...state, showLoader: true };
    }

    case CONSTANTS.DELETE_LIST_SUCCESS: {
      const updatedLists = action.payload.lists;
      console.log("UPDATEDLIST IN DELETE LIST SUCCESS...", updatedLists);
      return { ...state, lists: updatedLists, showLoader: false };
    }

    case CONSTANTS.DRAG_SUCCESSFUL: {
      // const newState = state.lists;
      // const {
      //   droppableIdStart,
      //   droppableIdEnd,
      //   droppableIndexStart,
      //   droppableIndexEnd,
      //   // draggableId,
      //   type,
      // } = action.payload;
      // //   DRAGGING LIST AROUND
      // if (type === "list") {
      //   const list = newState.splice(droppableIndexStart, 1);
      //   newState.splice(droppableIndexEnd, 0, ...list);
      //   return { lists: newState };
      // }
      // // in the same list
      // if (droppableIdStart === droppableIdEnd) {
      //   const list = state.lists.find((list) => droppableIdStart === list.id);
      //   const card = list.cards.splice(droppableIndexStart, 1);
      //   list.cards.splice(droppableIndexEnd, 0, ...card);
      // }
      // //   OTHER LIST
      // if (droppableIdStart !== droppableIdEnd) {
      //   // find the list where drag happened
      //   const listStart = state.lists.find(
      //     (list) => droppableIdStart === list.id
      //   );
      //   // pull out the card from this list
      //   const card = listStart.cards.splice(droppableIndexStart, 1);
      //   // find the list where drag happened
      //   const listEnd = state.find((list) => droppableIdEnd === list.id);
      //   // put the card in the new list
      //   listEnd.cards.splice(droppableIndexEnd, 0, ...card);
      // }

      return { ...state, lists: action.payload.lists, showLoader: false };
    }

    default:
      return state;
  }
};

export default listReducer;
