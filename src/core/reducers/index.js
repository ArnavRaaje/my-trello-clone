import { combineReducers } from "redux";
import boardReducer from "./boardReducers";
import listReducer from "./listReducers";
import userReducer from "./userReducers";

export default combineReducers({
  lists: listReducer,
  user: userReducer,
  boards: boardReducer,
});
