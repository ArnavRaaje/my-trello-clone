import { CONSTANTS } from "../actions";

const initialState = {
  currentUser: null,
  userErr: "",
};

const userReducer = (state = initialState, action) => {
  switch (action.type) {
    case CONSTANTS.LOG_IN_SUCCESS:
      return {
        ...state,
        currentUser: action.payload,
        userErr: "",
      };

    case CONSTANTS.USER_ERROR:
      return {
        ...state,
        userErr: action.payload,
      };

    case CONSTANTS.LOG_OUT_SUCCESS:
      return {
        ...state,
        ...initialState,
      };

    case CONSTANTS.SIGN_UP_SUCCESS:
      return { ...state, currentUser: action.payload, userErr: "" };

    default:
      return state;
  }
};

export default userReducer;
